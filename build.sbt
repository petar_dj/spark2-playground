name := "spark2-playground"

version := "0.1"

scalaVersion := "2.11.12"

val sparkVersion = "2.4.7"
val postgresVersion = "42.2.2"

libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  // streaming
  "org.apache.spark" %% "spark-streaming" % sparkVersion,
  // streaming-kafka
  "org.apache.spark" %% "spark-sql-kafka-0-10" % sparkVersion,
  // low-level integrations
  "org.apache.spark" %% "spark-streaming-kafka-0-10" % sparkVersion,
  "org.postgresql" % "postgresql" % postgresVersion
)
